extern crate temperature;
use std::str::FromStr;
use temperature::{Celsius, Fahrenheit, Kelvin, Rankine, Temperature};

fn main() {
    println!("{:?}", Fahrenheit::from_str("451"));
    println!("{}", Celsius::from_str("73.59").unwrap());

    //BelowAbsoluteZero frm_str
    match Kelvin::from_str("-0.3") {
        Ok(k) => println!("{}", k),
        Err(e) => println!("{}", e)
    }
    //BelowAbsoluteZero f64
    match Fahrenheit::new(-544.2) {
        Some(f)=> println!("{}", f),
        None => println!("None: Probably means BelowAbsoluteZero")
    }

    //ParsingFloatError
    match Kelvin::from_str("$44.2") {
        Ok(k) => println!("{}", k),
        Err(e) => println!("{}", e)
    }
    // Kelvin without ° (deg) symbol
    match Kelvin::new(23.0) {
        Some(k)=> println!("{}", k),
        None => println!("None: Probably means BelowAbsoluteZero")
    }

    // Todo: BelowAbsoluteZero returns None
    match Fahrenheit::new(-600.0) {
        Some(r)=> println!("{}", r),
        None => println!("None: Probably means BelowAbsoluteZero")
    }

    // value() returns value without symbol
    let k = Kelvin::new(9000.1).unwrap();
    println!("{}", k.value());

    // Display for Option not iplemented
    // println!("{}", Kelvin::new(32.0));

    // Convert from Temperature
    println!("Conversion at from absolute zero");
    println!("-459.67°F {:?}°C", Celsius::from(Fahrenheit::new(-459.67).unwrap()).value() );
    println!("0.0K      {:?}°C", Celsius::from(Kelvin::new(0.0).unwrap()).value() );
    println!("0.0°R     {:?}°C", Celsius::from(Rankine::new(0.0).unwrap()).value() );

    println!("-273.15°C {:?}°F", Fahrenheit::from(Celsius::new(-273.15).unwrap()).value() );
    println!("0.0K      {:?}°F", Fahrenheit::from(Kelvin::new(0.0).unwrap()).value() );
    println!("0.0°R     {:?}°F", Fahrenheit::from(Rankine::new(0.0).unwrap()).value() );
    
    println!("-273.15°C {:?}K", Kelvin::from(Celsius::new(-273.15).unwrap()).value() );
    println!("-459.67°F {:?}K", Kelvin::from(Fahrenheit::new(-459.67).unwrap()).value() );
    println!("0.0°R     {:?}K", Kelvin::from(Rankine::new(0.0).unwrap()).value() );
    
    println!("-273.15°C {:?}", Rankine::from(Celsius::new(-273.15).unwrap()).value() );
    println!("-459.67°F {:?}", Rankine::from(Fahrenheit::new(-459.67).unwrap()).value() );
    println!("0.0K      {:?}°R", Rankine::from(Kelvin::new(0.0).unwrap()).value() );
}

