extern crate temperature;
use temperature::{Celsius, Fahrenheit, Kelvin, Rankine};
use std::str::FromStr;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let temp: &str = args[1].as_ref();
    let from: &str = args[2].as_ref();
    let to: &str = args[3].as_ref();
 
    match (from, to) {
        ("C", "F") => println!("{}", Fahrenheit::from( Celsius::from_str(temp).unwrap() ) ),
        ("C", "K") => println!("{}", Kelvin::from( Celsius::from_str(temp).unwrap() ) ),
        ("C", "R") => println!("{}", Rankine::from( Celsius::from_str(temp).unwrap() ) ),
        ("F", "C") => println!("{}", Celsius::from( Fahrenheit::from_str(temp).unwrap() ) ),
        ("F", "K") => println!("{}", Kelvin::from( Fahrenheit::from_str(temp).unwrap() ) ),
        ("F", "R") => println!("{}", Rankine::from( Fahrenheit::from_str(temp).unwrap() ) ),
        ("K", "C") => println!("{}", Celsius::from( Kelvin::from_str(temp).unwrap() ) ),
        ("K", "F") => println!("{}", Fahrenheit::from( Kelvin::from_str(temp).unwrap() ) ),
        ("K", "R") => println!("{}", Rankine::from( Kelvin::from_str(temp).unwrap() ) ),
        ("R", "C") => println!("{}", Celsius::from( Rankine::from_str(temp).unwrap() ) ),
        ("R", "F") => println!("{}", Fahrenheit::from( Rankine::from_str(temp).unwrap() ) ),
        ("R", "K") => println!("{}", Kelvin::from( Rankine::from_str(temp).unwrap() ) ),
        _ => println!("Temperature unknown or feature not implemented. Valid units {{C|F|K|R}}\nExample: ./temperature 32 F C")
    };
}


