use std::num::ParseFloatError;

#[derive(Debug)]
pub enum Error {
    BelowAbsoluteZero,
    Parsing(ParseFloatError),
}

impl From<ParseFloatError> for Error {
    fn from(err: ParseFloatError) -> Self { Error::Parsing(err) } }

use std::fmt::Display;
impl Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        match self {
            Error::BelowAbsoluteZero => f.write_str("Error: BelowAbsoluteZero"),
            Error::Parsing(inner) => inner.fmt(f),
        }
    }
}