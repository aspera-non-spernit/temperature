#[macro_export]
/// Takes a series of numbers as str and returns them wrapped in a ```Vec<Decimal>```  \
/// If you want to use this utility macro, you have to add the ```rust_decimal``` crate \
/// into your Cargo.toml \
/// ``` use rust_decimal::Decimal;```
macro_rules! into_dec {
    ( $( $c:expr ),* ) => {
        {
            let mut consts: Vec<Decimal> = Vec::new();
            $(
                consts.push(Decimal::from_str($c).unwrap());
            )*
            consts
        }
    };
}

