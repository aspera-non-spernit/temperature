[![Build Status](https://travis-ci.org/aspera-non-spernit/temperature.svg?branch=master)](https://travis-ci.org/aspera-non-spernit/temperature)

# temperature 

*Temperature*, a library (and executable) written in Rust converts Temperature Units "Celsius", "Fahrenheit", "Kelvin", "Rankine" ```From``` eachother.

## Usage

### As Library ###

See examples at ```examples/simple.rs```

### Compile and run standalone program 

#### Compile and install
```bash
$ cargo build --release
$ strip ./target/release/temperature
$ chmod 711 ./target/release/temperature 
\# mv ./target/release/temperature /usr/local/bin/
```

*Temperature* takes three arguments without dash. 

1. The input temperature
2. The input temperature unit
3. The desired output temperature unit 

#### Execute
```bash
$ temperature {temp} {C|F|K|R} {C|F|K|R} 
```
#### Example
```bash
$ temperature 32.9 C F
```

## Contributing

- /r/rust Community on reddit
- https://github.com/fmease/ providing a better trait implementation using macros
